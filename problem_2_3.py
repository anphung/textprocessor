"""An application which opens a text file and calculates
the 5 most common words in the file. Morever, it can
find the number of occurrence of words that match a regex.
"""

import os
import sys
import re
import argparse
import itertools
import multiprocessing
from collections import Counter

from problem_1 import EmoParser


def read_input():
    """Parse command line arguments.

    Returns
    -------
    file_path : string
        File to be word counted.
    """
    parser = argparse.ArgumentParser(
                        description="Find the 5 most common words in file.")
    parser.add_argument("file_path", metavar="file_path", type=str,
                        help="Path to file to be word counted")
    parser.add_argument("--re", type=str,
                        help="find frequency of words match regex RE \
                        enclosed in \"\"")

    args = vars(parser.parse_args())
    return args["file_path"], args["re"]


def count_words(part):
    """Count words frequency from part["offset"]
    to part["offset"] + part["length"] in
    file part["file_path"].

    Paramaters
    ----------
    part: dict of string to int
        Information of a partition including file path,
        offset and length.

    Returns
    -------
    result : collections.Counter
        Counter that store words frequency.
    """
    # Open file and jump to location that needs to be counted
    f = open(part["file_path"], "r")
    f.seek(part["offset"])
    # Read whole partition
    str = f.read(part["length"])
    f.close()
    # Intialize parser and parse content
    emo_parser = EmoParser()
    words = emo_parser.parse(str)
    # Count word frequency using Collection.Counter
    # The reduce steps is included in this step.
    result = Counter(words)
    return result


def count_words_with_regex(part):
    """Count frequency of words that match regex part["pattern"]
    from part["offset"] to part["offset"] + part["length"] in
    file part["file_path"].

    Paramaters
    ----------
    part: dict of string to int
        Information of a partition including regex pattern,
        file path, offset and length.

    Returns
    -------
    result : collections.Counter
        Counter that store words frequency.
    """
    # Open file and jump to location that needs to be counted
    f = open(part["file_path"], "r")
    f.seek(part["offset"])
    # Read whole partition
    str = f.read(part["length"])
    f.close()
    # Intialize parser and parse content
    emo_parser = EmoParser()
    words = emo_parser.parse(str)
    # Filter and keep only words that match pattern
    words = [w for w in words if re.match(part["pattern"], w)]
    # Count word frequency using Collection.Counter
    # The reduce steps is included in this step.
    result = Counter(words)
    return result


class WordCounter:
    """This class counts word frequency in file. We
    use multiprocessing.Pool for paralling processing.
    We divide file into nearly equal size partition.
    Then we call each worker to count words those
    partition and accumulate result after all worker
    finishing their task.

    Future:
    We can scale this class easily because the workers process
    file by offset. With little modification, we could run
    this program on difference instant and modify difference
    party of the file. In count_words(), count_words_with_regex(),
    instead of returning result, we can write them into
    file system (or queue), and reduce them easily with
    reduce worker task.
    """

    def __init__(self, file_path, processes=None):
        """Initialize WordCount.

        Parameters
        ----------
        file_path : string
            Path to file to be counted.
        processes : int
            Number of processes (worker) to run.
        """
        self.file_path = file_path
        if processes:
            self.processes = processes
        else:
            self.processes = multiprocessing.cpu_count()
        self.pool = multiprocessing.Pool(processes)

    def file_partition(self):
        """We will partition file into number of partition to assign
        to each process. This method will calculate offset and size
        of each partition.

        Returns
        -------
        part : list of tuple
            File path, Offset and length of each partition
            for each process (worker).
        """
        # Get file size
        try:
            file_size = os.path.getsize(self.file_path)
        except Exception, e:
            print >> sys.stderr, "Can't read file"
            print >> sys.stderr, "Exception: %s" % str(e)
            sys.exit(1)

        # Calculate offsets
        f = open(self.file_path, "r")
        part = []
        offset = 0
        # Noce: Calculate partition size (in byte). In this case, each
        # worker processes 1 partition. If we want difference partition
        # size we can modify directly. Such as:
        # partition_size = 1000
        partition_size = file_size / self.processes
        while True:
            # Read partition
            lines = f.readlines(partition_size)
            if not lines:
                break
            # Convert lines list into string to get length of all
            # lines read.
            lines = "".join(lines)
            part.append({"file_path": self.file_path,
                         "offset": offset,
                         "length": len(lines)
                         })
            offset += len(lines)
        f.close()
        return part

    def words_frequency(self, pattern=None):
        """Count word frequency in file self.file_path.

        Returns
        -------
        result : collections.Counter
            Counter that store words frequency.
        """
        part = self.file_partition()
        if pattern:
            # Add pattern to arguments to pass to worker
            for x in part:
                x.update({"pattern": pattern})
            map_result = self.pool.map(count_words_with_regex, part)
        else:
            map_result = self.pool.map(count_words, part)
        result = Counter()
        for r in map_result:
            result = result + r
        return result

    def most_common(self, n):
        """Returns a list of tuples of n most common words
        and its frequency in file self.file_path.

        Parameters
        ----------
        n : int

        Returns
        -------
        common_words : list of tuple
            List of tuples such as [("a_word", 4), ("b_word", 2)] of most
            common words.
        """
        return self.words_frequency().most_common(n)

    def match(self, pattern):
        """Returns a list of tuples of words that match
        regex and its frequency in file self.file_path.

        Parameters
        ----------
        pattern : string
            Regex pattern.

        Returns
        -------
        words : list of tuple
            List of tuples such as [("a_word", 4), ("b_word", 2)]
            of word that match pattern.
        """
        return self.words_frequency(pattern).most_common()


def print_result(result):
    """Print result in a formatted way.

    Parameters
    ----------
    result : list of tuple
        List of tuple of word and its frequency.
    """
    if result:
        for word, frequency in result:
            print("%s %d" %(word, frequency))


def main():
    """Read file path and count 5 most common words in that file.
    """
    file_path, pattern = read_input()
    word_counter = WordCounter(file_path)
    if pattern:
        print_result(word_counter.match(pattern))
    else:
        print_result(word_counter.most_common(5))


if __name__ == "__main__":
    main()
