"""
Problem 4:
As a part of a web service monitoring application, write a function
that periodically polls a web address (www.wikipedia.com).
The expected response from the web address is HTTP status code 200.
Log the status with the time it took to make the query to stdout.
"""

import time
import sys
import argparse
import requests
import logging


def read_input():
    """Parse command line arguments.

    Returns
    -------
    addr : string
        Web address to be polled.
    period : float
        Poll period in second.
    """
    parser = argparse.ArgumentParser(
                        description="Periodically polls a web address.")
    parser.add_argument("addr", metavar="address", type=str,
                        help="web address to be polled")
    parser.add_argument("-p", "--period", metavar="period", type=float,
                        default=2.0, help="period in second (default 2)")

    args = vars(parser.parse_args())
    return args["addr"], args["period"]


def create_log():
    """Create a formatted log to stdout
    Source: https://coderwall.com/p/_fg97w/creating-a-python-logger-to-stdout

    Returns:
    log : logger
        The formmatted logger to stdout.
    """
    log = logging.getLogger(__name__)
    out_hdlr = logging.StreamHandler(sys.stdout)
    out_hdlr.setFormatter(logging.Formatter("%(asctime)s %(message)s"))
    out_hdlr.setLevel(logging.INFO)
    log.addHandler(out_hdlr)
    log.setLevel(logging.INFO)
    return log


def poll(addr, period):
    """After every period second(s), poll website addr and log
    status code, query time to stdout.

    Parameters
    ----------
    addr : string
        Web address to be polled.
    period : float
        Poll period in second.

    Returns
    -------
    No return
    """
    log = create_log()
    # Keep polling forever or until there is break signal
    while True:
        # Delay period second before each request
        time.sleep(period)
        # Initialize current time to measure the time it took for the
        # request
        start = time.time()
        # Process addr
        addr = addr.lower()
        if (addr.find("http://") == -1) and (addr.find("https://") == -1):
            addr = "http://" + addr

        try:
            # Start query. In case we want to query header,
            # change to request.head()
            resp = requests.get(addr,
                        # Remove the below line if dont want to redirect
                        allow_redirects=True,
                        # verify="", # Add path to SSL CAs
                        )
        except Exception as e:
            print(e)
            continue
        # Wait until all content is loaded.
        # Remove the below line if used requests.head() above
        # because we do not have to wait for the content
        # in that case.
        resp.content
        # Calculate round trip time
        query_time = time.time() - start

        # Log to stdout
        log.info("Status code: "
                + str(resp.status_code)
                + ". Request time: "
                + "{:.7}".format(str(query_time))
                + ".")


def main():
    """Read arguments (address, period) and poll that website."""
    address, period = read_input()
    poll(address, period)


if __name__ == "__main__":
    main()
