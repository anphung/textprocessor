"""Problem 1:
Given the input string: "Hello world! How (sp?) are you today (;"
split it into a list of words. You must strip all punctuation except
for emoticons. Result for example:
    ['Hello','world', 'how', 'sp', 'are', 'you', 'today', '(;']
Sample cases:
    "Hello :hug:::?:)"
    ":):):) Hey.How are you?(;)"
"""

import argparse
import string
import re


EMOTICONS_LST = [":)", ":-)", ":]", ":-]", ":3", ":-3", ":v", ":>", ":->" \
    , "8)", "8-)", ":}", ":-}", "(;", ":hug:", ":?"
    ]


def read_input():
    """Parse command line arguments.

    Returns
    -------
    str : string
        String to be processed.
    """
    parser = argparse.ArgumentParser(
                        description="Strip string.")
    parser.add_argument("str", metavar="string", type=str,
                        help="String to be processed, enclosed in \"\"")

    args = vars(parser.parse_args())
    return args["str"]


class EmoParser:
    """This class helps parsing string in to list of word.
    The class strips all punctuation except for emoticons.

    Future:
    This class can use multiprocessing.Pool for
    parallel proccessing.
    """

    def __init__(self, lst=EMOTICONS_LST):
        """Initialize emoticons list for parsing.

        Parameters
        ----------
        lst : list of string
            List of emoticons.
        """
        self.emoticons_lst = lst

    def parse(self, str):
        """Split string str into list of words. The list
        strips all punctuation except for emoticons.

        Parameters
        ----------
        str : string
            String to be processed.

        Returns
        -------
        result : list of string
            List of words from string str.
        """
        # We use regex to add space around emoticons to separatethem
        # n case they stick into words, or each other.
        # Such as: "Hello(;How", ":):)"
        emo_pattern = r"(" \
                    + r'|'.join(map(re.escape, self.emoticons_lst)) \
                    + r")"
        str = re.sub(emo_pattern, r" \1 ", str)
        # Split str into list of words
        lst = str.split()
        result = []
        # Traverse each word and strip punctuation
        for i in xrange(len(lst)):
            if lst[i] not in self.emoticons_lst:
                # Replace all punctuation with white space " "
                punc_pattern = "[%s]" % re.escape(string.punctuation)
                lst[i] = re.sub(punc_pattern, " ", lst[i])
                # We split lst[i] again because after stripping,
                # we may get some separate words. For example in string:
                # "The sky is blue.The ocean is also blue."
                # After stripping "blue.The", we have 2 words: "blue", "The".
                result.extend(lst[i].split())
            else:
                result.append(lst[i])

        return result


def main():
    """Read input and process."""
    str = read_input()
    emo_parser = EmoParser()
    print(emo_parser.parse(str))


if __name__ == "__main__":
    main()
